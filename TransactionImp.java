
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.util.*;

public class TransactionImp extends UnicastRemoteObject implements Transaction {

    String s;
    public static ArrayList<Block> blockchain = new ArrayList<Block>();
    private static TreeMap <Integer,ArrayList<Participant>> map;

    public TransactionImp() throws RemoteException {
        super();
    }

    public String getString() throws RemoteException {
        return s;
    }

    public void SendBlockChain(ArrayList blocktToSend) throws RemoteException {
        s = "Block send!";
        blockchain = blocktToSend;
    }

    public ArrayList RecieveBlockChain() throws RemoteException {
        s = "Block recieved!";
        return blockchain;
    }

    public void SendListParticipant(TreeMap m)  throws RemoteException {
        map = m;
    }

    public TreeMap  RecieveListParticipant()throws RemoteException{
        return map;
    }

}
