
import java.util.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Transaction extends Remote {

    public void SendBlockChain(ArrayList blocktToSend)
            throws RemoteException;

    public ArrayList RecieveBlockChain()
            throws RemoteException;

    public String getString() throws RemoteException;

    public void SendListParticipant(TreeMap m)  throws RemoteException ;

    public TreeMap  RecieveListParticipant()throws RemoteException; 
}
