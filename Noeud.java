import java.util.*;
import java.net.*;
import java.rmi.*;
import java.util.Random;
import java.util.Map.Entry;

public class Noeud extends TimerTask {

    private ArrayList<Block> blockchain;
    private ArrayList<Block> blockchainRecieved;
    private static ArrayList<Participant> waiting;
    public TreeMap <Integer,ArrayList<Participant>> map = new TreeMap(); 
    public TreeMap <Integer,ArrayList<Participant>> mapRecieve = new TreeMap();

    private String listenRMIID; // use to listen for incoming hashes
    private String broadcastRMIID; // use to broadcast hashes
    private static final int CONNECTION_PORT = 2099;
    public static final int DIFFICULTY = 4;
    private int blockCount = 0;
    private static Boolean dirtyFlag = false;
    private Transaction listenTransaction;
    int j = 0;
    int id;
 

    public Noeud(int id, String broadcastRMIID, String listenRMIID) {
        this.id = id;
        this.broadcastRMIID = broadcastRMIID;
        this.listenRMIID = listenRMIID;
        blockchain = new ArrayList();
        blockchainRecieved = new ArrayList();
        waiting = new ArrayList();
        if(dirtyFlag == false)
        {
            // Create only once the registry
            try {
                java.rmi.registry.LocateRegistry.createRegistry(CONNECTION_PORT);
            } catch (RemoteException re) {
                System.out.println(re);
            }
            dirtyFlag = true;
        }
    }

    public int nbParticipant() {
        Random rand = new Random();
        return (rand.nextInt(5) + 1);
    }

    public int getIdNoeud() {
        return id;
    }

    public void addBlock(Block b) {
        blockchain.add(b);
    }


    public void run() {
        ConnectServer();
        acceptConnection();
    }
    
   
    private void ConnectServer(){
        try {
            TransactionImp t = new TransactionImp();
            Naming.rebind("rmi://localhost:2099" + broadcastRMIID, t);
            System.out.println("Serveur pret");
            map.put(id,waiting);
            t.SendBlockChain(blockchain);
            t.SendListParticipant(mapRecieve);
            System.out.println(t.getString());
        } catch (RemoteException re) {
            System.out.println(re);
        } catch (MalformedURLException e) {
            System.out.println(e);
        }
    }
    

    public void acceptConnection(){   
        Boolean canAccept = true;
        try {
            listenTransaction = (Transaction) Naming.lookup("rmi://localhost:2099" + listenRMIID);            
            blockchainRecieved = listenTransaction.RecieveBlockChain();
            mapRecieve = listenTransaction.RecieveListParticipant();
            //
            boolean flag = false;
            for (Entry<Integer,ArrayList<Participant>> e1 : mapRecieve.entrySet())
		    {
                if( e1.getKey() == id )
                {
                    flag = true;
                }
            }
            if( flag == false)
            {
                mapRecieve.put(id,waiting);
            }

            for (Entry<Integer,ArrayList<Participant>> e1 : mapRecieve.entrySet())
            {
                System.out.println("map" + e1.getValue());
            }


        } catch (NotBoundException re) {
            System.out.println(re);
            canAccept = false;
        } catch (RemoteException re) {
            System.out.println(re);
            canAccept = false;
        } catch (MalformedURLException e) {
            System.out.println(e);
            canAccept = false;
        }
        
        if(canAccept == false)
        {
            return;
        }
                
        try {
            blockchainRecieved = listenTransaction.RecieveBlockChain();
            System.out.println("Received = " + blockchainRecieved.size());
        } catch (RemoteException re) {
            System.out.println(re);
        }
        
		if(j == 0)
		{
			this.addBlock(new Block("0"));
			blockchain.get(0).mineBlock(DIFFICULTY);
			this.GiveBlockValue();
			j++;
		}

		else
		{
            int count = 0;
            //SEND RECIEVE
            for (Entry<Integer,ArrayList<Participant>> e1 : mapRecieve.entrySet())
            {
                count+= e1.getValue().size(); 
            }

            int randSend;
            int randRecieve;
            double valueToSend = 0;
            do
            {
                Random rand = new Random();
                randSend = (rand.nextInt(count) + 1);
                randRecieve = (rand.nextInt(count) + 1);

            }while(randSend == randRecieve);

            for (Entry<Integer,ArrayList<Participant>> e : mapRecieve.entrySet())
            {
                for(int i=0 ; i < e.getValue().size() ; i++)
                {
                    if(e.getValue().get(i).id == randSend - 1)
                    {
                        Participant p = e.getValue().get(randSend-1);
                       
                        System.out.println("****************SENDING**************** ");
                        System.out.println("PARTICIPANT "+ p.id +" AVAIT "+ Double.parseDouble(String.format("%.3f", p.getValue())) + " Coins");
                        valueToSend = p.sendValue();
                        System.out.println("PARTICIPANT " + p.id +" A MAINTENANT "+ Double.parseDouble(String.format("%.3f", p.getValue())) + " Coins");
                        System.out.println("*************************************");
                    }
                }


                for(int i=0 ; i < e.getValue().size() ; i++)
                {
                    if(e.getValue().get(i).id == randRecieve - 1)
                    {
                        System.out.println("****************RECIEVING*************");
                        Participant p = e.getValue().get(randRecieve-1);
                        System.out.println("PARTICIPANT "+ p.id +" AVAIT "+ Double.parseDouble(String.format("%.3f", p.getValue())) + " Coins");
                        p.setValue(p.getValue() + valueToSend);
                        System.out.println("PARTICIPANT "+ p.id +" A MAINTENANT "+ Double.parseDouble(String.format("%.3f", p.getValue())) + " Coins");
                        System.out.println("*************************************");
                    }
                }
               
            }
			System.out.println("hashes " + j );
			this.addBlock(new Block(blockchain.get(blockchain.size()-1).getHash()));
			blockchain.get(blockchain.size()-1).mineBlock(DIFFICULTY);
			this.GiveBlockValue();
			j++;
        }

        for(Participant p : waiting)
		{
			System.out.println("p" + p.id +": "+ Double.parseDouble(String.format("%.3f", p.getValue())));
		}
        System.out.println("\nBlockchain is Valid: " + isChainValid());

		if(blockchainRecieved.size() >= blockchain.size())
		{
		  blockchain = blockchainRecieved;
        }
        
		for(Block b : blockchain)
		{
			System.out.println("block hash:" + b.getHash());
		}
        System.out.println("\nBlockchain is Valid: " + isChainValid());
        ConnectServer();
    }

    public Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = new String(new char[DIFFICULTY]).replace('\0', '0');

        //loop through blockchain to check hashes:
        for (int i = 1; i < blockchain.size(); i++) 
        {
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i - 1);
            //compare registered hash and calculated hash:
            if (!currentBlock.getHash().equals(currentBlock.computeHash())) {
                System.out.println("Current Hashes not equal");
                return false;
            }
            //compare previous hash and registered previous hash
            if (!previousBlock.getHash().equals(currentBlock.previousHash)) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved does it start with   n 0s
            if (!currentBlock.getHash().substring(0, DIFFICULTY).equals(hashTarget)) {
                System.out.println("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }

    public void addwaiting(Participant p) {
        waiting.add(p);
    }

    public void createParticipant(int nbParticipant, Noeud n) {
        System.out.println(nbParticipant);
        for (int i = 0; i < nbParticipant; i++) {
            Random r = new Random();
            double randomValue = 1.0 + (5.0 - 1.0) * r.nextDouble();
            new Participant(i,0.0, randomValue).ConectionParticipant(n);
        }
    }

    public void GiveBlockValue() {
        double someM = 0.0;//somemerite

        for (Participant pa : waiting){
            someM += pa.getM();
        }
      
        for (Participant pa : waiting) {

            pa.setValue((pa.getValue()) + ( pa.getM()*(1./someM) )) ;
        }
    }

    public static void main(String[] argv) {

        Timer time = new Timer();
        String broadcast = "/t";
        String listen = "/t1";
        
        if(argv.length > 1)
        {
            broadcast = argv[0];
            listen = argv[1];
        }
        Noeud n = new Noeud(Integer.parseInt(argv[2]), broadcast,listen);
        n.createParticipant(n.nbParticipant(),n);
        time.schedule(n, 0, 10000);// tous les 10 sec
       
    }
}
