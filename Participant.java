
import java.util.Random;
import java.io.Serializable;

public class Participant implements Serializable {

    private double value;
    private double M;
    int id;
    int idblock;

    public Participant(int id,double value, double M) {
        this.value = value;
        this.id = id;   
        this.M = M;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double d) {
        value = d;
    }

    public double getM()
    {
        return M;
    }

    public double sendValue() {

        double start = 0.0;
        double end = getValue();
        double random = new Random().nextDouble();
        double result = start + (random * (end - start));
        setValue(getValue() - result);
        return result;
    }

    public void ConectionParticipant(Noeud N) {
        idblock = N.getIdNoeud();
        N.addwaiting(this);
    }
}
