
import java.util.*;
import java.security.MessageDigest;
import java.io.Serializable;

public class Block implements Serializable {

    private String hash; //id
    public String previousHash;
    private long timeStamp;
    private int nonce;

    public Block(String previousHash) {
        this.previousHash = previousHash;
        timeStamp = new Date().getTime();
        hash = computeHash();
    }

    protected static String hashSha1(String str) {
        StringBuffer sb = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String getHash() {
        return hash;
    }

    public String computeHash() {
        String hashBlock = hashSha1(previousHash + Long.toString(timeStamp) + Integer.toString(nonce));
        return hashBlock;
    }

    public void mineBlock(int difficulty) {
        String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0" 
        while (!hash.substring(0, difficulty).equals(target)) {
            nonce++;
            hash = computeHash();
        }
        System.out.println("Block Mined!!! : " + hash);
    }
}
